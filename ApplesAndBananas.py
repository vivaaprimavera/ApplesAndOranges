#!/usr/bin/env python3

from pprint import pprint
import tensorflow

from tensorflow import keras

from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten,Reshape
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.layers import SeparableConv1D

from keras.layers import Concatenate,concatenate

from keras.utils import plot_model



import numpy
import pandas
import math

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


from trainingUtils import DataSequence
import trainingUtils.MLBlocks as MLBlocks
from pprint import pprint

dataFile = 'data/source/2000.npy'

trainPercentage = 0.7
validationPercentage = 0.15
testPercentage = 0.15

batchSize = 8

data = numpy.load(dataFile,allow_pickle=True)

dataSize = data.shape[0]

trainNumber = int(trainPercentage*dataSize)
validationNumber = int(validationPercentage*dataSize)
testNumber = int(trainPercentage*dataSize)

tr = DataSequence.DataSequence(pandas.DataFrame(data[0:trainNumber]),batchSize)
vr = DataSequence.DataSequence(pandas.DataFrame(data[trainNumber+1:trainNumber+validationNumber+1]),batchSize)
te = DataSequence.DataSequence(pandas.DataFrame(data[-testNumber:]),batchSize)

def doPlot(hist,fname):
    x = hist.history
    toPlotI = {}
    for k in x.keys():
        toPlotI[k] = [not math.isnan(y) for y in x[k] ]
    toPlot = {}
    for k in toPlotI.keys():
        dec = True
        for item in toPlotI[k]:
            dec = dec and item
        if dec:
            toPlot[k] = dec
    print(f'''plot debug {x}''')
    for h in toPlot.keys():
        plt.plot(x[h])
    plt.title(f'''model {fname}''')
    plt.ylabel('values')
    plt.xlabel('epoch')
    plt.legend([y for y in toPlot.keys()],bbox_to_anchor=(1.05,1), loc='upper left')
    try:
        plt.savefig(f'''{fname}.eps''',bbox_inches='tight')
    except Exception:
        print(f'''{fname}.png''')
        print(Exception)
    plt.clf()


def createModel():
    s = Input(shape=(5000,))
    #
    # Since the input size is 5000 the convolutions will be in size/1000,size/100 and size/10
    #
    iout = MLBlocks.ParallelConv1D(s,((2,1),(4,2),(8,8),(16,16)))
    #pout0 = MLBlocks.ParallelConv1D(iout,((2,1),(4,2),(8,8),(16,16)))
    sepconv = SeparableConv1D(2,2,name='GeneralSignalProcessing')(iout)

    signalPreprocessor = Dense(1024,activation='relu',name='SignalPreprocessor')(Flatten()(sepconv))
    

    #sfp = Dense(256,activation='relu',name='SignalFinderProcessor')([signalPreprocessor,adj] )
    sfp = Dense(256,activation='relu',name='SignalFinderProcessor')(signalPreprocessor )
    

    sfpi = Dense(512,activation='relu')(sfp)


    yn = Dense(2,activation='softmax',name='SignalFinderOutput')(sfpi)

    # Already saw that convulutions are effective for pulse measurement

    #sc0 = MLBlocks.ParallelConv1D(pout1,((32,1),(64,5),(32,50),(16,500)))
    #sc1 = MLBlocks.ParallelConv1D(sc0,((32,1),(64,5),(32,50),(16,500)))
    #sc2 = MLBlocks.ParallelConv1D(sc1,((32,1),(64,5),(32,50),(16,500)))


    # The information from SignalPreprocessor is also important

    #irf = Concatenate(axis=1)([yn,d,Flatten()(sc2)])
    sepconvCarcact = SeparableConv1D(2,2,name='CaracterizationProcessing')(iout)  
    irf = Concatenate(axis=1)([yn,Flatten()(sepconvCarcact),signalPreprocessor]) 

    adjl = Dense(512,activation='relu',name='SignalCaracterization')(irf)

    #adjl = Dropout(sbrlx0,name='Chair0')(adjl)

    cpl = Dense(256,activation='relu')(adjl)
    
    #cpl = Dropout(sbrlx0,name='Chair1')(cpl)
     
    adj = Dense(1,activation='linear',name='CaracterizationOutput')(cpl)
    
    model = Model(inputs=s,outputs=[yn,adj])
    model.summary()
    plot_model(model,'model.png',show_layer_activations=True,show_shapes=True)
    return model



model = createModel()
model.compile(optimizer='nadam',
    #loss=['binary_crossentropy',keras.losses.MeanSquaredError()],
    loss=['binary_crossentropy',keras.losses.MeanAbsoluteError()],
    loss_weights = [0.25,250],
    #loss=['binary_crossentropy','mse'],
    metrics=['accuracy',keras.metrics.MeanSquaredError(),keras.losses.MeanAbsoluteError()],
    )

history = model.fit(tr,
    epochs=2,
    steps_per_epoch = int(len(tr.values)/batchSize), 
    validation_steps = int(len(vr.values)/batchSize), 
    validation_data=vr,
    max_queue_size=1, 
    verbose = 1
    )    

pprint(history)

try:
    doPlot(history,dataFile)
except Exception:
    pass

model.save(f'''{dataFile}.h5''')

results = model.evaluate(te,batch_size=4)

pprint(results)
