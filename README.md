# ApplesAndOranges

Test for a signal classifier network

This "thing" is the result of a Data Chalange in the field of particle Physics


## Name
Apples and Bananas

## Description
Proof of concept NN that can:

Identify one of two signals
Measure one caracteristic that is present in only one of them 

## Installation
requirements:

tensorflow
numpy
pandas
matplotlib
cython
setuptools


Preparation

run ./build.sh   to build the required pythom package (will be installed in current dir, does not mess up your current python install)

## Usage
Train the network with ApplesAndBananas.py, validate.py is still a work in progress

## License
If something is used in a academic paper, contact and cite.

## Project status
Ongoing when time permits
