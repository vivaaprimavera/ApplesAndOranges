#!/usr/bin/python3

from setuptools import setup,Extension
from Cython.Build import cythonize,build_ext

import os


REQUIREMENTS = [
    "numpy",
    "cython",
    "tensorflow",
    "pandas"
]

def ext_modules():
    import numpy

    include_dirs = ['.','src', numpy.get_include()]
    root_dir = os.path.abspath(os.path.dirname(__file__))
    DataSequence = Extension(
        "DataSequence",
        sources=[root_dir + "/src/DataSequence.pyx"],
        include_dirs=include_dirs,
        extra_compile_args=['-fopenmp'],
        extra_link_args=['-fopenmp']
    )
    Utilities = Extension(
        "Utilities",
        sources=[root_dir + "/src/utilities.pyx"],
        include_dirs=include_dirs,
        extra_compile_args=['-fopenmp'],
        extra_link_args=['-fopenmp']
    )
    MLBlocks = Extension(
        "MLBlocks",
        sources = [root_dir + "/src/MLBlocks.pyx"],
        include_dirs=include_dirs,
        extra_compile_args=['-fopenmp'],
        extra_link_args=['-fopenmp']
    )
    exts = [DataSequence, Utilities,MLBlocks]
    return cythonize(exts)

setup(name='Training Utils',
    ext_package="trainingUtils",
    ext_modules=ext_modules(),
    cmdclass={"build_ext" : build_ext})
