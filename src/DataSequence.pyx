#cython: language_level=3

import cython

from tensorflow.keras.utils import Sequence
from tensorflow.keras.preprocessing import image

import numpy
cimport numpy
import math
import tensorflow

from trainingUtils.Utilities import running_in

def NormalizeData(x):
    mx = numpy.max(x) 
    mi = numpy.min(x)
    return ((x- mi)/ (mx - mi))

class DataSequence(Sequence):
    """
    Keras Sequence object to train a model on larger-than-memory data.
    """
    @running_in
    def __init__(self, df, batch_size, mode='train'):
        print(df)
        self.df = df
        #self.df.astype('float32')
        self.bsz = batch_size # batch size
        self.mode = mode # shuffle when in train mode

        # Take labels and a list of image locations in memory
        self.values = numpy.array(self.df[1])
        self.labels = numpy.array(self.df[0])

    @running_in
    def __len__(self):
        # compute number of batches to yield
        return int(math.ceil(len(self.df) / float(self.bsz)))

    @running_in
    def get_batch_labels(self, idx):
        # Fetch a batch of labels
        signalClass = []
        peakDistanceClass = []
        for v in self.labels[idx * self.bsz: (idx + 1) * self.bsz]:
            #signalClass.append( numpy.array([v[0],v[1]]) )
            #peakDistanceClass.append( numpy.array(v[3]) )
            signalClass.append( [v[0],v[1]] )
            peakDistanceClass.append( numpy.array([v[3]]) )
            
            #print(f'''v is {v} it is {it} ''')
        #retval.astype(float32)
        #r = numpy.array(t)
        #print(f'''error here {r.shape}''')
        r =  ( numpy.array(signalClass) , numpy.array(peakDistanceClass) )
        #print(f''' r = {r} '{r[0].shape} {r[1].shape} ''')
        return r


    @running_in
    def get_batch_features(self, idx):
        # Fetch a batch of inputs
        #train_image = []
        #cdef str entry
        #cdef numpy.ndarray tmpImg
        #for entry in self.im_list[idx * self.bsz: (1 + idx) * self.bsz]:
        #    img =  getImage(entry)
        #    if not img:
        #        exit(1)
        #    img = image.img_to_array(img)
        #    img = img/255
        #    tmpImg = numpy.array(img)
        #    #tmpImg = tmpImg.astype('float16')
        #    train_image.append(tmpImg)
        #cdef numpy.ndarray ret = numpy.array(train_image)
        retval = self.values[idx * self.bsz: (1 + idx) * self.bsz]
        t = []
        for v in retval:
            b = NormalizeData(numpy.array(v))
            print(f'''------> {type(b)} {type(v)}''')
            t.append(b)
        r = numpy.array(t)
        #retval.astype(float32)
        return r


    @running_in
    def __getitem__(self, idx):
        print(f'''\nindex: {idx} ''')
        batch_x = self.get_batch_features(idx)
        batch_y = self.get_batch_labels(idx)
        #print('-------------------------------')
        #print(type(batch_x))
        #print([type(x) for x in batch_x])
        #print('-------------------------------')
        #print(type(batch_y))
        #print([x for x in batch_y])
        #print(f'''{batch_x.shape} {batch_y.shape}''')
        return ( batch_x, batch_y  )
