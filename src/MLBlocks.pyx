#cython: language_level=3

import cython

#cimport tensorflow
import tensorflow

from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.layers import BatchNormalization
from keras.layers import Concatenate,concatenate
from keras.layers import Flatten

def ParallelConv1D(layer_in,args=((32,1),(64,5),(32,50),(16,500))):
	c0 = Conv1D(args[0][0],args[0][1],padding = 'same',activation = 'relu')(layer_in)
	c1 = Conv1D(args[0][0],args[0][1],padding = 'same',activation = 'relu')(layer_in)
	c2 = Conv1D(args[1][0],args[1][1],padding = 'same',activation = 'relu')(layer_in)
	c3 = Conv1D(args[2][0],args[2][1],padding = 'same',activation = 'relu')(layer_in)
	out = Concatenate()([c0,c1,c2,c3])
	return out

def AResBlock1D(layer_in,args=((8,16),(8,16)),batchNormalize=False):
	v0 = Conv1D(args[0][0],args[0][1],activation='relu')(layer_in)
	if batchNormalize:
		v0 = BatchNormalization()(v0)
	v2 = Conv1D(args[1][0],args[1][1],activation='relu')(v0)
	if batchNormalize:
		v2 = BatchNormalization()(v2)
	return v2


####
# Historic reasons
def special_case_loss(y_true,y_pred):
	print(f'''-> {y_true} {y_pred.shape} ''')
	#print(f'''----> {y_true} {y_pred} ''')
	signalLen_t = tensorflow.gather(y_true,[2])
	signalLen_p = tensorflow.gather(y_pred,[2])
	yn_t = tensorflow.gather(y_true,[0,1])
	yn_p = tensorflow.gather(y_pred,[0,1])
	bce = tensorflow.keras.losses.BinaryCrossentropy(from_logits=True)
	cdef inl = bce(yn_t,yn_p)
	mse = tensorflow.keras.losses.MeanSquaredError()
	cdef float cl = mse(signalLen_t,signalLen_p)
    #print(f''' loss: {inl} {cl}''')
    #t = numpy.array([inl,cl]) 
	int = tensorflow.stack([inl,cl])
	cdef float t = tensorflow.math.reduce_mean(int)
	#print(f'''loss value {t} ''')
	return t
