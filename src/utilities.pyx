#cython: language_level=3

import cython
from functools import wraps
import time


from random import shuffle

def running_in(func):
    @wraps(func)
    def wrap(*args, **kw):
        ts = time.time()
        cdef object result
        result = func(*args, **kw)
        te = time.time()
        print('func:%r args:[%r, %r] took: %2.6f sec' % (func.__name__, args, kw, te-ts))
        return result
    return wrap
